## VSCode Sample C Project
This is a sample C project for VSCode. It includes debugging profiles and build tasks for both MINGW and Linux. Feel free to use it for your own project.

To compile and debug on MINGW, download MSYS2 from https://www.msys2.org/ and install it. Then install the nessasary packages by running ` pacman -S mingw-w64-x86_64-gcc  mingw-w64-i686-gcc  mingw-w64-x86_64-make  mingw-w64-i686-make  mingw-w64-x86_64-gdb  mingw-w64-i686-gdb`.

To compile and debug on Linux, install `gcc gdb make`.